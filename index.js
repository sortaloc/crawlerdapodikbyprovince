'use strict'

var express     =   require('express');
var app         =   express();
var path        =   require('path');

const axios =   require('axios');
const fs    =   require('fs')

console.log(process.argv[2])
const   idWilayah   =   process.argv[2]
const groupBy = (xs, key)=> {
    return xs.reduce(function(rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

const getKota   =   (idWilayah)=>{
    axios.get('https://dapo.dikdasmen.kemdikbud.go.id/rekap/progres?&id_level_wilayah=1&kode_wilayah='+idWilayah+'&semester_id=0').then(res=>{
        // console.log(res.data)
        res.data.map(i=>{
            console.log(i.nama)
            getKecamatan(i.kode_wilayah)
        })
    }).catch(err=>{
        console.log({err})
    })
}

const getKecamatan  =   (idKota)=>{
    axios.get('https://dapo.dikdasmen.kemdikbud.go.id/rekap/progres?&id_level_wilayah=2&kode_wilayah='+idKota+'&semester_id=0').then(res=>{
        // console.log(res.data)
        res.data.map(i=>{
            // console.log({parent:i.induk_kabupaten,nama:i.nama})
            getSekolah(i.kode_wilayah)
        })
    }).catch(err=>{
        console.log({err})
    })
}

const s = []
const getSekolah    =   (idKec)=>{
    axios.get('https://dapo.dikdasmen.kemdikbud.go.id/rekap/progresSP?id_level_wilayah=3&semester_id=0&kode_wilayah='+idKec).then(res=>{
        // console.log(res.data)
        res.data.map((i)=>{
            // console.log({parentKabupaten:i.induk_kabupaten,nama:i.nama,npsn:i.npsn,jumlahPesertaDidik:i.pd,ruangKelas:i.jml_rk,jenjang:i.bentuk_pendidikan,statusSekolah:i.status_sekolah,indukKecamatan:i.induk_kecamatan,rombonganBelajar:i.rombel,jumlahGuru:i.ptk,jumlahPegawai:i.pegawai,jumlahPerpus:i.jml_perpus,jumlahLab:i.jml_lab})
            s.push({no:s.length+1,parentKabupaten:i.induk_kabupaten,nama:i.nama,npsn:i.npsn,jumlahPesertaDidik:i.pd,ruangKelas:i.jml_rk,jenjang:i.bentuk_pendidikan,statusSekolah:i.status_sekolah,indukKecamatan:i.induk_kecamatan,rombonganBelajar:i.rombel,jumlahGuru:i.ptk,jumlahPegawai:i.pegawai,jumlahPerpus:i.jml_perpus,jumlahLab:i.jml_lab})
        })
        console.log('jumlah seluruh Sekolah '+ s.length)
        // console.log(groupBy(s, 'parentKabupaten'));
        // const d =   groupBy(s, 'parentKabupaten')
        fs.writeFile('outputJSON/hasil-'+idWilayah+'.json', JSON.stringify(s), 'utf8',function(err){
            if (err){throw err}else{
            console.log('data tersimpan');}
          }); 
    }).catch(err=>{
        console.log({err})
    })
}
// console.log(groupBy(s, 'parentKabupaten'));

getKota(idWilayah)

app.use('/laporan', express.static(path.join(__dirname + '/hasil.json')));

app.listen(5000,()=>{
    console.log('Running . . . .',new Date())
})